import * as ExtractTextPlugin from 'extract-text-webpack-plugin'
import * as autoprefixer from 'autoprefixer'

export function createTestConfig(sourcePath: string) {
    return {
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: [
                        {
                            loader: 'react-hot-loader/webpack',
                        },
                        {
                            loader: 'awesome-typescript-loader',
                            options: {
                                configFileName: 'tsconfig.webpack.json',
                            }
                        }
                    ]
                },
                {
                    enforce: 'post',
                    test: /\.(js|tsx?)$/,
                    loader: 'istanbul-instrumenter-loader',
                    include: sourcePath,
                    exclude: [
                        /\.d.ts$/,
                        /\.(e2e|spec)\.tsx?$/,
                        /node_modules/
                    ]
                },
                { // sass / scss loader for webpack
                    test: /\.s?css$/,
                    use: [
                        "style-loader",
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: {
                                includePaths: [sourcePath]
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                plugins: function () {
                                    return [autoprefixer]
                                }
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin({
                filename: "[name].[contenthash].css",
                disable: true
            })
        ]
    }
};
import * as webpack from 'webpack'
import * as autoprefixer from 'autoprefixer'
import { CheckerPlugin } from 'awesome-typescript-loader'

export function createDevConfig(sourcePath: string) {
    return {
        module: {
            rules: [
                // .ts, .tsx
                {
                    test: /\.tsx?$/,
                    use: [{
                        loader: 'react-hot-loader/webpack',
                    },
                    {
                        loader: 'awesome-typescript-loader',
                        options: {
                            configFileName: 'tsconfig.webpack.json',
                        }
                    }]
                },
                { // sass / scss loader for webpack
                    test: /\.s?css$/,
                    use: [
                        "style-loader",
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: {
                                includePaths: [sourcePath]
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                plugins: function () {
                                    return [autoprefixer]
                                }
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new CheckerPlugin(),            
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                filename: 'vendor.bundle.js',
                minChunks: Infinity
            })
        ],
        devServer: {
            disableHostCheck: true,
            contentBase: sourcePath,
            historyApiFallback: true,
            inline: true,
            open: true,
            stats: {
                assets: false,
                cached: false,
                cachedAssets: false,
                children: false,
                chunks: false,
                chunkModules: false,
                chunkOrigins: false,
                colors: true,
                depth: false,
                entrypoints: false,
                errors: true,
                errorDetails: false,
                hash: false,
                maxModules: 0,
                modules: false,
                performance: false,
                providedExports: false,
                publicPath: false,
                reasons: false,
                source: false,
                timings: false,
                usedExports: false,
                version: false,
                warnings: false
            }
        }
    }
};
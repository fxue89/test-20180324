import * as testWebpackConfig from './webpack.config'

// Currently custom *.d.ts files (that are outside of node_modules/@types) are not picked during a test run
// https://github.com/s-panferov/awesome-typescript-loader/issues/468
// workaround is to modify tsconfig `"files": [ "./src/typings/custom-typings.d.ts" ]`

module.exports = config => {
    config.set({
        basePath: '',

        frameworks: ['jasmine'],

        files: [{
                pattern: 'tests/**/*.spec.ts'
            },
            {
                pattern: 'tests/**/*.spec.tsx'
            }
        ],

        client: {
            captureConsole: true,
            logLevel: config.LOG_DEBUG
        },

        preprocessors: {
            // add webpack as preprocessor
            'tests/**/*.tsx': ['webpack', 'sourcemap'],
            'tests/**/*.ts': ['webpack', 'sourcemap']
        },

        webpack: testWebpackConfig,

        coverageReporter: {
            reporters: [
                //{type: 'html', dir:'coverage/'},  // https://github.com/karma-runner/karma-coverage/issues/123
                {
                    type: 'text'
                },
                {
                    type: 'text-summary'
                }
            ],
        },

        // Webpack please don't spam the console when running in karma!
        webpackMiddleware: {
            quiet: true,
            stats: {
                colors: true
            }
        },

        plugins: [
            'karma-webpack',
            'karma-jasmine',
            //'karma-sourcemap-writer',
            'karma-sourcemap-loader',
            'karma-coverage',
            //'karma-remap-istanbul',
            //'karma-spec-reporter',
            'karma-chrome-launcher',
        ],

        reporters: ['progress', 'coverage'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['ChromeHeadless'],
        customLaunchers: {
            ChromeHeadless: {
                base: 'Chrome',
                flags: [
                    '--no-sandbox',
                    '--headless',
                    '--disable-gpu',
                    '--remote-debugging-port=9222' // won't run without these settings
                ]
            }
        },
        singleRun: true,
    });
};
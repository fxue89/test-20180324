import * as React from 'react'
import { ClientContainer } from 'components/Client';

export function Start() {
    return (
        <div className="full-screen-description">
            <div>
                Find you advertising options
            </div>
            <ClientContainer />
        </div>
    )
}
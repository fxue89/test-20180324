import * as React from 'react'
import { Link } from "react-router-dom"
import { ReactNode } from 'react'
import * as classNames from 'classnames';

export const LinkButton = props =>
    <Link className="btn btn-lg center"
        {...props}
        onClick={() => {
            window.scrollTo(0, 0);
            if (props.onClick) {
                props.onClick();
            }
        }}
        to={props.disabled ? "#" : props.to}>
        {props.children}
    </Link>

export const ForwardButton = (props: { className?: string, onClick?: () => void, children?: ReactNode }) =>
    <button
        type={props.onClick ? "button" : "submit"}
        className={classNames("btn btn-lg", props.className)}
        onClick={() => {
            window.scrollTo(0, 0);
            if (props.onClick) {
                props.onClick();
            }
        }}>
        {props.children}
    </button>

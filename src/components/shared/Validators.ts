import { Address } from '../../store/app/steps/Details2'
import { DateOnly } from "core/DateOnly"

export const required = value => value != undefined && value !== ''

const isEmptyDate = (value?: Partial<DateOnly>) => (value == undefined || (value.day == undefined && value.month == undefined && value.year == undefined))

export const dateRequired = (value?: Partial<DateOnly>) => !isEmptyDate(value)

export const dateValid = (value?: Partial<DateOnly>) => {
    if (isEmptyDate(value)) {
        return true; // do not trigger if optional
    }

    const date = new Date(value!.year!, value!.month! - 1, value!.day!);

    if (isNaN(date.getTime())) {
        return false;
    }

    return date.getDate() == value!.day!
        && date.getMonth() == (value!.month! - 1)
        && date.getFullYear() == value!.year;
}

export const yearBetween = (minYear: number, maxYearExclusive: number) => (value?: Partial<DateOnly>) => {
    if (isEmptyDate(value) || !dateValid(value)) {
        return true; // do not trigger if optional
    }

    return value!.year! >= minYear && value!.year! < maxYearExclusive;
}

const emailRegexp =
    /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;

export const email = (value: string) => {
    if (!required(value)) {
        return true;
    }
    if (value != undefined && emailRegexp.test(value)) {
        const dotIndex: number = value.split('@')[1].indexOf(".");
        return dotIndex > -1 && value.substring(value.length - 1) != ".";
    } else {
        return false;
    }
}

const isEmptyPhone = (value: string) => value == undefined || value == ''
export const phoneRequired = (value: string) => !isEmptyPhone(value)

const auMobileCodeRegexp = /^04/
const auAreaCodeRegexp = /^0(2|3|4|7|8)/

export const auMobileLength = (value: string) => {
    if (isEmptyPhone(value)) {
        return true; // do not trigger if optional
    }
    return value != undefined && value.length === 10
}
export const auPhoneLength = (value: string) => {
    if (isEmptyPhone(value)) {
        return true; // do not trigger if optional
    }
    return value != undefined && value.length === 10
}
export const auMobile = (value: string) => {
    if (isEmptyPhone(value) || value.length < 10) {
        return true; // do not trigger if optional
    }
    return value != undefined && auMobileCodeRegexp.test(value)
}
export const auPhone = (value: string) => {
    if (isEmptyPhone(value) || value.length < 10) {
        return true; // do not trigger if optional
    }
    return value != undefined && auAreaCodeRegexp.test(value)
}
export const phoneSet = (value: { mobile: string, home: string, work: string }) => {
    const mobileIsFilled = required(value.mobile)
    const phoneIsFilled = (value: string) => required(value)
    if (mobileIsFilled || phoneIsFilled(value.home) || phoneIsFilled(value.work)) {
        return true;
    }
    return false;
}
const auPostCodeRegexp = /^\d{4}$/

export const auPostCode = (value: string) => !required(value) || auPostCodeRegexp.test(value)

export const autocompleteInputRequired = (value: Partial<Address>) => required(value.input)

export const autocompleteSelectionRequired = (value: Partial<Address>) => !autocompleteInputRequired(value) || !!(value.lat && value.lng) 

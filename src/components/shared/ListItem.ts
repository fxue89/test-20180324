export interface ListItem<T> {
    readonly text: string
    readonly value: T
}
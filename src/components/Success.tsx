import * as React from 'react'
import { LinkButton } from 'components/shared/LinkButton';
import { RouteMap } from 'components/RouteMap';
import { ProductCartContainer } from 'components/products/ProductCart';

export function Success() {
    return (
        <div className="full-screen-description">
            <div>
                Thanks for shopping with us.
                <br/>
                Here is your products:
                <ProductCartContainer isCartOpen={true} />                
            </div>
            <LinkButton to={RouteMap.ProductsSelect}>
                Review
            </LinkButton>
        </div>
    )
}
import * as React from 'react'
import { ProductType } from 'store/app/products/ProductsState';
import { AppState } from 'store/app/AppState';
import { connect } from 'react-redux';

interface Props {
    isCartOpen: boolean
    products: ProductType[]
    toggleCart: (isCartOpen: boolean) => void
}

class ChartButton extends React.Component<Props> {
    render() {
        return (
            <button onClick={() => this.props.toggleCart(this.props.isCartOpen)} className="cart-button">
                {this.props.isCartOpen ? 'Close' : 'Cart'}
                {this.props.products.length > 0 &&
                    <span>{this.props.products.length}</span>
                }
            </button>
        )
    }
}

function mapStateToProps(state: AppState) {
    return {
        products: state.products.products
    }
}

export const ChartButtonContainer = connect(mapStateToProps)(ChartButton)
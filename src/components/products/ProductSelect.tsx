import './ProductSelect.scss'
import * as React from 'react'
import { ProductDetails } from 'data/ProductDetails';
import { ProductType, ProductsState } from 'store/app/products/ProductsState';
import { Dispatch, connect } from 'react-redux';
import { ProductsActionType } from 'store/app/products/productsReducer';
import { ClientType } from 'store/app/client/clientReducer';
import { AppState } from 'store/app/AppState';
import { SpecialOffer } from 'data/SpecialOffers';
import classNames = require('classnames');
import { Link } from 'react-router-dom';
import { RouteMap } from 'components/RouteMap';

interface Props {
    client: ClientType
    dispatch: Dispatch<ProductsState>
}

export class ProductSelect extends React.Component<Props> {
    private addProduct = (product: ProductType) => {
        this.props.dispatch({ type: ProductsActionType.Add, product })
    }

    render() {
        const specialOfferClient = SpecialOffer.find(clientOffer => clientOffer.client === this.props.client)
        const clientProductDetails = ProductDetails.map(product => {
            return {
                ...product,
                offer: specialOfferClient ? specialOfferClient.offers.find(offer => offer.product === product.type) : undefined
            }
        })
        return (
            <div className="container-inner product-select">
                {
                    clientProductDetails.map((product, index) => (
                        <div key={index} className="product-container">
                            <div className="details">
                                <div className="product-name">{product.name}</div>
                                <div className="product-description">{product.description}</div>
                                <div className={
                                    classNames([
                                        "product-price",
                                        (product.offer && product.offer.toPrice && !product.offer.fromAmount) && "stroke"
                                    ])}>$ {product.price.toString()}</div>
                                {(product.offer && product.offer.toPrice && !product.offer.fromAmount) &&
                                    <div className="special-offer">Special: $ {product.offer.toPrice.toString()}</div>}

                                {(product.offer && product.offer.fromAmount && product.offer.toPrice && !product.offer.toAmount) &&
                                    <div className="special-offer">
                                        $ {product.offer.toPrice.toString()} when purchase more than {product.offer.fromAmount}
                                    </div>}
                                {(product.offer && product.offer.fromAmount && !product.offer.toPrice && product.offer.toAmount) &&
                                    <div className="special-offer">
                                        Special: Get {product.offer.fromAmount} for {product.offer.toAmount}
                                    </div>}
                            </div>
                            <div>
                                <button className="btn btn-primary" onClick={() => this.addProduct(product.type)}>Add</button>
                            </div>
                        </div>
                    ))
                }
                <div className="route-links">
                    <Link className="btn btn-sm" to={RouteMap.Root}>Back</Link>
                    <Link className="btn btn-sm" to={RouteMap.Complete}>Next</Link>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: AppState) {
    return {
        client: state.client.client
    }
}

function mapDispatchToProps(dispatch: Dispatch<ProductsState>) {
    return { dispatch };
}

export const ProductSelectContainer = connect(mapStateToProps, mapDispatchToProps)(ProductSelect)
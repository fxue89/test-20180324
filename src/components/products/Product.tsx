import './Product.scss'
import * as React from 'react'
import { ProductSelectContainer } from 'components/products/ProductSelect';
import { ProductCartContainer } from 'components/products/ProductCart';
import { ChartButtonContainer } from 'components/CartButton';

interface State {
    isCartOpen: boolean
}

export class Product extends React.Component<{}, State> {
    state = { isCartOpen: false }
    private toggleCart = (isCartOpen: boolean) => {
        this.setState({ isCartOpen: !isCartOpen })
    }
    render() {
        return (
            <div className="product">
                <ProductSelectContainer />
                <ProductCartContainer isCartOpen={this.state.isCartOpen} />
                <ChartButtonContainer isCartOpen={this.state.isCartOpen} toggleCart={this.toggleCart} />
            </div>
        )
    }
}
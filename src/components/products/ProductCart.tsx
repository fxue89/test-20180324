import './ProductCart.scss'
import * as React from 'react'
import { ProductType } from 'store/app/products/ProductsState';
import { AppState } from 'store/app/AppState';
import { connect } from 'react-redux';
import { ClientType } from 'store/app/client/clientReducer';
import { SpecialOffer } from 'data/SpecialOffers';
import { checkoutHelper } from 'core/checkoutHelper';
import classNames = require('classnames');

interface Props {
    isCartOpen: boolean
    client: ClientType
    products: ProductType[]
}

export class ProductCart extends React.Component<Props> {
    render() {
        const products = checkoutHelper.getProductsdetails(this.props.products)
        const specialOfferClient = SpecialOffer.find(clientOffer => clientOffer.client === this.props.client)
        const { cartTotal, saving } = checkoutHelper.getTotalAndSaving(specialOfferClient, products)
        return (
            <div className={classNames(["product-cart", this.props.isCartOpen && "open"])}>
                <div className="container-inner">
                    <div className="product-list">
                        {
                            products.map((product, index) => {
                                return product.amount > 0 &&
                                    <div key={index}>
                                        <div>{product.name}</div>
                                        <div>{product.amount}</div>
                                    </div>
                            })
                        }
                    </div>
                    <div className="total">Total: $ {cartTotal.toFixed(2)}</div>
                    {saving > 0.1 && <div className="saving">Saved: $ {saving.toFixed(2)}</div>}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: AppState) {
    return {
        client: state.client.client,
        products: state.products.products
    }
}

export const ProductCartContainer = connect(mapStateToProps)(ProductCart)
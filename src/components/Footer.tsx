import * as React from 'react'

export class Footer extends React.Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="container-outer center">
                    <div className="flex items-center">
                        A small React/Redux project developed by Faye
                    </div>
                </div>
            </footer>
        )
    }
}
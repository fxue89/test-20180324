export enum RouteMap {
    Root = "/",
    Client = "/client",
    ProductsRoot = "/products",
    ProductsSelect = "/products/select",
    Complete = "/success"
}
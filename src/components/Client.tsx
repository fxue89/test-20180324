import 'css/controls.scss'
import './Client.scss'
import * as React from 'react'
import { ClientType, ClientState, ClientActionType } from 'store/app/client/clientReducer';
import { Dispatch, connect } from 'react-redux';
import { RouteMap } from 'components/RouteMap';
import { Link } from 'react-router-dom';

interface Props {
    dispatch: Dispatch<ClientState>
}

interface State {
    matchingClient: ClientType[]
}
export class Client extends React.Component<Props, State> {
    state = { matchingClient: [] }

    private inputElement: HTMLInputElement | null = null
    private readonly handleInputRef = (ref: HTMLInputElement | null) => this.inputElement = ref
    private getClient = () => {
        const clientName = this.inputElement ? this.inputElement.value : '';
        const getClientType = Object.keys(ClientType).reduce((client, currentValue) => {
            return ClientType[currentValue] === clientName.toLowerCase() ? ClientType[currentValue] : client
        }, ClientType.Default)
        this.props.dispatch({ type: ClientActionType.Set, client: getClientType })
    }

    private handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const inputValue = event.target.value.trim().toLowerCase()
        const matchingClient = Object.keys(ClientType).filter(client => {
            const currentClient = ClientType[client].toString().toLowerCase()
            return (currentClient.includes(inputValue) && inputValue !== '' && client !== 'Default')
        }).map(client => ClientType[client])
        this.setState({ matchingClient })
    }

    private setInputValue = (match: ClientType) => {
        if (this.inputElement) {
            this.inputElement.value = match
        }
        this.setState({ matchingClient: [] })
    }

    render() {
        return (
            <div className="container-inner client">
                <div className="label">
                    Company Name
                </div>
                <div className="control">
                    <input type="text" ref={this.handleInputRef} onChange={this.handleChange} />
                    <span className="control-underline"></span>
                    {this.state.matchingClient.length > 0 &&
                        <div className="dropdown-container">
                            {
                                this.state.matchingClient.map((match, index) => (
                                    <div key={index} onClick={() => this.setInputValue(match)}>{match}</div>
                                ))
                            }
                        </div>
                    }
                </div>

                <div>
                    <Link to={RouteMap.ProductsSelect}>Skip</Link>
                </div>

                <Link className="btn btn-primary" onClick={this.getClient} to={RouteMap.ProductsSelect}>Next</Link>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch: Dispatch<ClientState>) {
    return { dispatch };
}

export const ClientContainer = connect(undefined, mapDispatchToProps)(Client)
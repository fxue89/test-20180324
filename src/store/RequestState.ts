export type Milliseconds = number

export enum RequestStateType {
    Pending, Success, Error
}

export type RequestState<T> = { readonly creationDate?: Milliseconds } &
    (
        | {
            readonly type: RequestStateType.Pending,
        }
        | {
            readonly type: RequestStateType.Success,
            readonly data: T
        }
        | {
            readonly type: RequestStateType.Error,
            readonly message: string
        }
    )
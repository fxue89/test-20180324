import { applyMiddleware, combineReducers, createStore, Store } from 'redux'
import logger from 'redux-logger'
import { History } from 'history'
import { routerMiddleware, routerReducer } from 'react-router-redux'
import thunk from 'redux-thunk';
import { AppState } from "store/app/AppState"
import { productsReducer } from 'store/app/products/productsReducer';
import { ClientReducer } from './app/client/clientReducer';

interface WindowWithTools {
    devToolsExtension?(): (args?: any) => any;
}

export class StoreHelper {
    constructor(
        private readonly history: History,
        private readonly useLogger = true) { }

    createAndConfigureStore(defaultState: AppState = AppState.Initial): Store<AppState> {
        const windowWithTools = window as WindowWithTools;
        const historyMiddleware = routerMiddleware(this.history);

        const create = windowWithTools.devToolsExtension
            ? windowWithTools.devToolsExtension()(createStore)
            : createStore;

        const reducers = combineReducers<AppState>({
            products: productsReducer,
            client: ClientReducer
        } as any);

        const enhanceStoreCreator = this.useLogger ? applyMiddleware(historyMiddleware, logger, thunk) : applyMiddleware(historyMiddleware, thunk);
        const createStoreWithMiddleware = enhanceStoreCreator<AppState>(create);
        return createStoreWithMiddleware(reducers, defaultState) as Store<AppState>;
    }
}
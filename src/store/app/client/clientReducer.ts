export enum ClientType {
    Default = 'default',
    Unilever = 'unilever',
    Apple = 'apple',
    Nike = 'nike',
    Ford = 'ford'
}

export interface ClientState {
    client: ClientType
}


export const ClientState = {
    Initial: {
        client: ClientType.Default
    }
}


export enum ClientActionType {
    Set = 'ClientActionType.Set'
}

export type ClientAction = 
| {
    readonly type: ClientActionType.Set
    readonly client: ClientType
}


export function ClientReducer(state: ClientState = ClientState.Initial, action: ClientAction): ClientState {
    switch (action.type) {
        case ClientActionType.Set:
            return { client: action.client }
        default:
            return state
    }
}
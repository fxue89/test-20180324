import { ProductType, ProductsState } from "./ProductsState";


export enum ProductsActionType {
    Add = 'ProductsActionType.Add'
}

export type ProductsAction = 
| {
    readonly type: ProductsActionType.Add
    readonly product: ProductType
}


export function productsReducer(state: ProductsState = ProductsState.Initial, action: ProductsAction): ProductsState {
    switch (action.type) {
        case ProductsActionType.Add:
            return { products: state.products.concat([action.product]) }
        default:
            return state
    }
}
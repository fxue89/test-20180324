export enum ProductType {
    Classic = 'classic',
    Standout = 'standout',
    Premium = 'premium'
}

export interface ProductsState {
    products: ProductType[]
}

export const ProductsState = {
    Initial: {
        products: []
    }
}
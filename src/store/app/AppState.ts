import { ProductsState, ProductType } from "./products/ProductsState";
import { ClientState, ClientType } from "./client/clientReducer";

export interface AppState {
    readonly products: ProductsState
    readonly client: ClientState
}

export const AppState = {
    Initial: {
        products: ProductsState.Initial,
        client: ClientState.Initial
    } as AppState,
    Sample: {
        products: {
            products: [ProductType.Classic, ProductType.Standout]
        },
        client: {
            client: ClientType.Apple
        }
    } as AppState
}

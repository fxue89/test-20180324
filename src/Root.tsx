import * as React from 'react'
import { Provider } from "react-redux"
import { ConnectedRouter } from "react-router-redux"
import { Switch, Route, RouteProps } from "react-router"
import { Store } from "redux"
import { AppState } from "store/app/AppState"
import { RouteMap } from 'components/RouteMap'
import { History } from 'history'
import { Footer } from 'components/Footer'
import { Start } from 'components/Start';
import { Success } from 'components/Success';
import { Product } from 'components/products/Product';

interface RootProps {
    store: Store<AppState>
    history: History
}

export function Root(props: RootProps) {
    return (
        <Provider store={props.store}>
            <ConnectedRouter history={props.history} >
                <main>
                    <div className="main-wrapper">
                        <Switch>
                            <Route exact path={RouteMap.Root} render={Start} />
                            <Route path={RouteMap.ProductsSelect} render={() => <Product />} />
                            <Route exact path={RouteMap.Complete} render={Success} />
                        </Switch>
                    </div>
                    <Footer />
                </main>
            </ConnectedRouter>
        </Provider>
    )
}
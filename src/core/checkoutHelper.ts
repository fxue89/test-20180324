import { ProductType } from "store/app/products/ProductsState";
import { ProductDetails, ProductDetail } from "data/ProductDetails";
import { ClientOffers } from "data/SpecialOffers";

export interface ProductSummary {
    type: ProductType,
    amount: 0
}

export const checkoutHelper = {
    getProductsdetails(products: ProductType[]): (ProductSummary & ProductDetail)[] {
        let productSummary: ProductSummary[] = [
            { type: ProductType.Classic, amount: 0 },
            { type: ProductType.Standout, amount: 0 },
            { type: ProductType.Premium, amount: 0 }
        ]
        productSummary = products.reduce((products, currentProduct) => {
            products.find(product => product.type === currentProduct)!.amount += 1
            return products
        }, productSummary)

        return productSummary.map(product => {
            return Object.assign(product, ProductDetails.find(item => item.type === product.type))
        })
    },
    calcProductTotal(price, amount, fromAmount?, toAmount?, toPrice?) {
        const startAmount = fromAmount ? fromAmount : 1;
        const endAmount = toAmount ? toAmount : 1;
        const actualPrice = (amount >= startAmount && toPrice && !toAmount) ? toPrice : price

        if (toAmount) {
            return (Math.floor(amount / startAmount) * endAmount + amount % startAmount) * actualPrice
        } else {
            return amount * actualPrice
        }
    },
    calcTotal(subTotals: number[]): number {
        return subTotals.reduce((total, cur) => (total += cur), 0)
    },
    getTotalAndSaving(specialOfferClient: ClientOffers | undefined, products: (ProductSummary & ProductDetail)[]) {
        const cartProductDetails = products.map(product => {
            const offer = specialOfferClient ? specialOfferClient.offers.find(offer => offer.product === product.type) : undefined
            return { ...product, offer }
        })

        const cartProductSubtotal = cartProductDetails.map(product => {
            return product.offer ?
                checkoutHelper.calcProductTotal(product.price, product.amount, product.offer.fromAmount, product.offer.toAmount, product.offer.toPrice) :
                checkoutHelper.calcProductTotal(product.price, product.amount)
        })

        const cartProductNoDiscountSubtotal = cartProductDetails.map(product => {
            return checkoutHelper.calcProductTotal(product.price, product.amount)
        })

        const cartTotal = checkoutHelper.calcTotal(cartProductSubtotal)
        const total = checkoutHelper.calcTotal(cartProductNoDiscountSubtotal)
        const saving = total - cartTotal
        return { cartTotal, saving }
    }
}
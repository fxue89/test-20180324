import { ClientType } from "store/app/client/clientReducer";
import { ProductType } from "store/app/products/ProductsState";

export interface Offer {
    product: ProductType
    fromAmount?: number
    toAmount?: number
    toPrice?: number
}

export interface ClientOffers {
    client: ClientType
    offers: Offer[]
}

export const SpecialOffer: ClientOffers[] = [
    {
        client: ClientType.Unilever,
        offers: [
            {
                product: ProductType.Classic,
                fromAmount: 3,
                toAmount: 2
            }
        ]
    },
    {
        client: ClientType.Apple,
        offers: [
            {
                product: ProductType.Standout,
                toPrice: 299.99
            }
        ]
    },
    {
        client: ClientType.Nike,
        offers: [
            {
                product: ProductType.Premium,
                fromAmount: 4,
                toPrice: 379.99
            }
        ]
    },
    {
        client: ClientType.Ford,
        offers: [
            {
                product: ProductType.Classic,
                fromAmount: 5,
                toAmount: 4
            },
            {
                product: ProductType.Standout,
                toPrice: 309.99
            },
            {
                product: ProductType.Premium,
                fromAmount: 3,
                toPrice: 389.99
            }
        ]
    }
]
import { ProductType } from "store/app/products/ProductsState";


export interface ProductDetail {
    type: ProductType
    name: string
    description: string
    price: number
}
export const ProductDetails: ProductDetail[] = [
    {
        type: ProductType.Classic,
        name: 'Classic Ad',
        description: 'offers the most basic level of advertisement',
        price: 269.99
    },
    {
        type: ProductType.Standout,
        name: 'Standout Ad',
        description: 'allows advertisers to use a company logo and use a longer presentation text',
        price: 322.99
    },
    {
        type: ProductType.Premium,
        name: 'Premium Ad',
        description: 'same benefits as Standout Ad, but also puts the ad at the top of the results, allowing  higher visibility',
        price: 394.99
    }
]
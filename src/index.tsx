import './index.scss'

import 'core-js/fn/array/find'
import 'core-js/fn/array/find-index'
import 'core-js/fn/string/includes'
import 'core-js/fn/promise'
import 'core-js/fn/object/assign'

import * as React from 'react'
import * as ReactDOM from 'react-dom'
import createHistory from 'history/createBrowserHistory'

import { appSettings } from "appSettings"

import { AppState } from 'store/app/AppState'
import { StoreHelper } from 'store/StoreHelper'
import { Root } from 'Root'

const history = createHistory({ basename: appSettings.basePath });
const storeHelper = new StoreHelper(history);

const initialState = AppState.Initial;
const store = storeHelper.createAndConfigureStore(initialState);

ReactDOM.render(<Root store={store} history={history} />, document.getElementById('root'));

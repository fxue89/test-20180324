/**
 * Helper utils for working with components.
 */

import { Simulate } from "react-dom/test-utils"

export const SimUtils = {
    setDescendantInputValue(parent: Element, placeholder: string, value: string) {
        const input = this.ensureInput(parent, placeholder);
        this.setInputValue(input, value);
    },

    setInputValue(input: HTMLInputElement, value: string) {
        input.value = value;
        Simulate.change(input);
        Simulate.input(input, { target: { value } } as any);
    },

    /**
     * Sets the value of a custom dropdown consisting of an input and a list
     * @param parent 
     * @param placeholder 
     * @param itemText 
     */

    selectDropdownItemByText(parent: Element, placeholder: string, itemText: string) {
        const input = this.ensureInput(parent, placeholder);
        const list = input.parentElement!.parentElement!.getElementsByTagName('ul')[0];
        expect(list).toBeDefined('list for '+ placeholder);
        const items = Array.from(list.getElementsByTagName('li'));
        const itemToSelect = items.find(x => x.innerText === itemText)!;
        expect(itemToSelect).toBeDefined(`selecting item ${itemText} from a list of: ${items.map(x => x.innerText).join()}`);
        Simulate.mouseDown(itemToSelect);
        expect(input.value).toBe(itemText);
    },

    ensureInput(parent: Element, placeholder: string): HTMLInputElement {
        const input = parent.querySelector(`input[placeholder="${placeholder}"]`) as HTMLInputElement;
        expect(input).toBeTruthy(placeholder);
        return input;
    },

    toggleCheckbox(checkbox: HTMLInputElement) {
        checkbox.checked = ! checkbox.checked;
        Simulate.change(checkbox, { target: checkbox, currentTarget: checkbox } as any);
    },

    selectRadio(parent:HTMLDivElement, text: string) {
        const options = Array.from(parent.getElementsByTagName('button'));
        const optionToSelect = options.find(x => x.innerText === text)!;
        Simulate.click(optionToSelect);
    }
}
import * as React from 'react'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from "react-router-redux"
import { Provider } from "react-redux"
import * as ReactTestUtils from 'react-dom/test-utils'
import { Simulate } from "react-dom/test-utils"
import { StoreHelper } from 'store/StoreHelper';
import { ProductSelect } from 'components/products/ProductSelect';
import { ClientType } from 'store/app/client/clientReducer';
import { ProductType } from 'store/app/products/ProductsState';

const client = ClientType.Default;
const products = [ProductType.Classic, ProductType.Standout, ProductType.Premium]

describe('Product select component', function () {
    const history = createHistory();
    const storeHelper = new StoreHelper(history);
    const store = storeHelper.createAndConfigureStore();

    function dispatch(dispatch: any) {
        store.dispatch(dispatch)
    }

    const root = ReactTestUtils.renderIntoDocument(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <div>
                    <ProductSelect client={client} dispatch={dispatch} />
                </div>
            </ConnectedRouter>
        </Provider>
    ) as React.Component;

    it('add products', function () {
        const productSelector = ReactTestUtils.findRenderedDOMComponentWithClass(root, 'product-select');
        const productList = productSelector.getElementsByClassName('product-container');
        Object.keys(products).forEach(key => {
            Array.from(productList).forEach(productElement => {
                const productNameElement = productElement.querySelector('.product-name') as HTMLElement
                const productName = productNameElement.innerText;
                console.log(products[key])
                if (productName.toLowerCase().includes(products[key])) {
                    const addProductButton = productElement.querySelector('button')!
                    Simulate.click(addProductButton);
                }
            })
        })

        const productsState = store.getState().products.products;

        productsState.forEach((product,index)=> {
            expect(product).toBe(products[index]);
        })
    });
});

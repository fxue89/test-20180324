import * as React from 'react'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from "react-router-redux"
import { Provider } from "react-redux"
import * as ReactTestUtils from 'react-dom/test-utils'
import { StoreHelper } from 'store/StoreHelper';
import { ClientType } from 'store/app/client/clientReducer';
import { ProductCart } from 'components/products/ProductCart';
import { ProductType } from 'store/app/products/ProductsState';

const client = ClientType.Default;
const products = [ProductType.Classic, ProductType.Standout, ProductType.Premium]
const total = 987.97

describe('Product cart component', function () {
    const history = createHistory();
    const storeHelper = new StoreHelper(history);
    const store = storeHelper.createAndConfigureStore();

    const root = ReactTestUtils.renderIntoDocument(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <div>
                    <ProductCart client={client} products={products} isCartOpen={true} />
                </div>
            </ConnectedRouter>
        </Provider>
    ) as React.Component;

    it('displays correct total value', function () {
        const productCart = ReactTestUtils.findRenderedDOMComponentWithClass(root, 'product-cart');
        const totalElement = productCart.getElementsByClassName('total')[0] as HTMLElement;
        const displayedTotal = parseFloat(totalElement.innerText.split('$')[1])
        expect(displayedTotal).toBe(total);
    });
});

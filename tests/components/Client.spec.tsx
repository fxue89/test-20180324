import * as React from 'react'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from "react-router-redux"
import { Provider } from "react-redux"
import * as ReactTestUtils from 'react-dom/test-utils'
import { StoreHelper } from 'store/StoreHelper';
import { ClientType } from 'store/app/client/clientReducer';
import { Client } from 'components/Client';
import { SimUtils } from '../SimUtils';
import { Simulate } from "react-dom/test-utils"

const client = 'Apple';

describe('Client component', function () {
    const history = createHistory();
    const storeHelper = new StoreHelper(history);
    const store = storeHelper.createAndConfigureStore();

    function dispatch(dispatch: any) {
        store.dispatch(dispatch)
    }

    const root = ReactTestUtils.renderIntoDocument(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <div>
                    <Client dispatch={dispatch} />
                </div>
            </ConnectedRouter>
        </Provider>
    ) as React.Component;

    it('set client type', function () {
        const clientElement = ReactTestUtils.findRenderedDOMComponentWithClass(root, 'client');
        const clientInput = clientElement.querySelector('input') as HTMLInputElement;
        SimUtils.setInputValue(clientInput, client);

        const nextButton = clientElement.querySelector('.btn') as HTMLElement;
        Simulate.click(nextButton);

        const clientState = store.getState().client.client;

        expect(clientState).toBe(ClientType.Apple);
    });
});

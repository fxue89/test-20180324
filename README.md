### Install packages

* Run the following command to install packages:
* **$ yarn install**

---

### Run project

* Run the following command to run project in dev mode:
* **$ yarn start**
* 
* Run the following command to run project in production mode:
* **$ yarn start:prod**

---

### Project structure

* **index.tsx**
* **Root.tsx** - React router route chooser
* **assets** - images, fonts
* **components** - all UI components
* **core** - Major helper functions
* **css** - shared css and css variables
* **data** - Service funtions, data mapping and static data to assist front-end rendering
* **store** - React redux store, state, reducer

---

### Questions
* Contact [fxue89@gmail.com](mailto:fxue89@gmail.com)